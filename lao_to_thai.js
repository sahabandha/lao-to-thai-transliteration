const prompt = require('prompt');

prompt.start();

prompt.get(['text'], function (err, result) {
  var text = result.text;

  text = text.replace(/ເຫລາ/g, "ເຫ ລາ")
    .replace(/ເຫຍ/g, "เหีย")
    .replace(/ຫງ/g, "໠")
    .replace(/ຫຍ/g, "໡")
    .replace(/ຫຼ/g, "໢")
    .replace(/ຫວ/g, "໣")
    .replace(/ກວ/g, "\u0EE6")
    .replace(/ຂວ/g, "\u0EE7")
    .replace(/ຄວ/g, "\u0EE8")
    .replace(/ອໍ່/g, "ອອ")
    .replace(/ອໍ/g, "ອໍ່")
    .replace(/ີ່ຍຽ/g, "ີ່ ຍຽ")
    .replace(/ີ້ຍຽ/g, "ີ້ ຍຽ")
    .replace(/ີ໊ຍຽ/g, "ີ໊ ຍຽ")
    .replace(/ີ໋ຍຽ/g, "ີ໋ຍ ຽ")
    .replace(/ີຍຽ/g, "ີຍ ຽ")
    .replace(/ຍ໌/g, "ย์");

  const replacements = [{
      pattern: /ເ([\u0E80-\u0EE5])ຍ/g,
      replacement: 'ເ$1ຽ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])([\u0EC8-\u0ECB])ຍ/g,
      replacement: 'ເ$1$2ຽ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ຼຍ/g,
      replacement: 'ເ$1ຼຽ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ຼ([\u0EC8-\u0ECB])ຍ/g,
      replacement: 'ເ$1ຼ$2ຽ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ີຍ/g,
      replacement: 'ເ$1ຍ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ີ([\u0EC8-\u0ECB])ຍ/g,
      replacement: 'ເ$1$2ຍ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ັ/g,
      replacement: 'ເ$1็'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ຼັ/g,
      replacement: 'ເ$1ຼ็'
    },
    {
      pattern: /ແ([\u0E80-\u0EE5])ັ/g,
      replacement: 'ແ$1็'
    },
    {
      pattern: /ແ([\u0E80-\u0EE5])ຼັ/g,
      replacement: 'ແ$1ຼ็'
    },
    {
      pattern: /็([\u0E80-\u0EE5])/g,
      replacement: '$1'
    },
    {
      pattern: /ັອ/g,
      replacement: '็อ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ີ/g,
      replacement: 'ເ$1ິ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ຼີ/g,
      replacement: 'ເ$1ຼິ'
    },
    {
      pattern: /ຽ/g,
      replacement: 'ຽย'
    },
    {
      pattern: /([\u0EC8-\u0ECB])ຽ/g,
      replacement: 'ຽ$1'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ຽ/g,
      replacement: 'ເ$1ີ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ຼຽ/g,
      replacement: 'ເ$1ຼີ'
    },
    {
      pattern: /([\u0E80-\u0EE5])ຼຽ/g,
      replacement: 'ເ$1ຼີ'
    },
    {
      pattern: /([\u0E80-\u0EE5])ຽ/g,
      replacement: 'ເ$1ີ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ິ([\u0EC8-\u0ECB])([\u0E80-\u0EAF\u0EDC-\u0EE5])/g,
      replacement: 'เ$1ิ$2$3'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ຼິ([\u0EC8-\u0ECB])([\u0E80-\u0EAF\u0EDC-\u0EE5])/g,
      replacement: 'เ$1ຼิ$2$3'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ິ([\u0EC8-\u0ECB])/g,
      replacement: 'เ$1$2อ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ຼິ([\u0EC8-\u0ECB])/g,
      replacement: 'เ$1ຼ$2อ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ິ/g,
      replacement: 'เ$1อ'
    },
    {
      pattern: /ເ([\u0E80-\u0EE5])ຼິ/g,
      replacement: 'เ$1ຼอ'
    }
  ];

  for (const replacement of replacements) {
    text = text.replace(replacement.pattern, replacement.replacement);
  }

  text = text.replace(/ກ/g, "ก")
    .replace(/ຂ/g, "ข")
    .replace(/຃/g, "ฃ")
    .replace(/ຄ/g, "ค")
    .replace(/຅/g, "ฅ")
    .replace(/ຆ/g, "ฆ")
    .replace(/ງ/g, "ง")
    .replace(/ຈ/g, "จ")
    .replace(/ຉ/g, "ฉ")
    .replace(/ຊ/g, "ช")
    .replace(/ຌ/g, "ซ")
    .replace(/໥/g, "ฌ")
    .replace(/ຎ/g, "ญ")
    .replace(/໤/g, "ฎ")
    .replace(/ຏ/g, "ฏ")
    .replace(/ຐ/g, "ฐ")
    .replace(/ຑ/g, "ฑ")
    .replace(/ຓ/g, "ณ")
    .replace(/ດ/g, "ด")
    .replace(/ຕ/g, "ต")
    .replace(/ຖ/g, "ถ")
    .replace(/ທ/g, "ท")
    .replace(/ນ/g, "น")
    .replace(/ບ/g, "บ")
    .replace(/ປ/g, "ป")
    .replace(/ຜ/g, "ผ")
    .replace(/ຝ/g, "ฝ")
    .replace(/ພ/g, "พ")
    .replace(/ຟ/g, "ฟ")
    .replace(/ຠ/g, "ภ")
    .replace(/ມ/g, "ม")
    .replace(/ຍ/g, "ย")
    .replace(/ຣ/g, "ร")
    .replace(/ລ/g, "ล")
    .replace(/ວ/g, "ว")
    .replace(/ຨ/g, "ศ")
    .replace(/ຩ/g, "ษ")
    .replace(/ສ/g, "ส")
    .replace(/ຫ/g, "ห")
    .replace(/ຬ/g, "ฬ")
    .replace(/ອ/g, "อ")
    .replace(/ຮ/g, "ฮ")
    .replace(/ໜ/g, "หน")
    .replace(/ໝ/g, "หม")
    .replace(/ໞ/g, "ก")
    .replace(/ໟ/g, "ย")
    .replace(/໠/g, "หง")
    .replace(/໡/g, "หย")
    .replace(/໢/g, "หล")
    .replace(/໣/g, "หว")
    // .replace(/ຢ່/g, "อย่")
    // .replace(/ຢ້/g, "ย่")
    // .replace(/ຢູ່/g, "อยู่")
    // .replace(/ຢູ້/g, "ยู่")
    // .replace(/ຢິ່/g, "หยิ")
    // .replace(/ຢິ້/g, "ยิ่")
    // .replace(/ຢີ່/g, "หยี่")
    // .replace(/ຢີ້/g, "ยี่")
    // .replace(/ຢາก/g, "อยาก")
    // .replace(/ຢາด/g, "หยาด")
    // .replace(/ຢາบ/g, "หยาบ")
    // .replace(/ຢ/g, "ย")
    // above for g**gle translate speaking
    .replace(/ຢ/g, "อย")
    .replace(/ະ/g, "ะ")
    .replace(/ັ/g, "ั")
    .replace(/າ/g, "า")
    .replace(/ຳ/g, "ำ")
    .replace(/ິ/g, "ิ")
    .replace(/ີ/g, "ี")
    .replace(/ຶ/g, "ึ")
    .replace(/ື/g, "ื")
    .replace(/ຸ/g, "ุ")
    .replace(/ູ/g, "ู")
    .replace(/຺/g, "ฺ")
    .replace(/ົ່ว/g, "ั่ว")
    .replace(/ົ້ว/g, "ั้ว")
    .replace(/ົ໊ว/g, "ั๊ว")
    .replace(/ົ໋ว/g, "ั๋ว")
    .replace(/ົว/g, "ัว")
    .replace(/ົ/g, "")
    .replace(/ຼ/g, "ล")
    .replace(/ເເ/g, "แ")
    .replace(/ເ/g, "เ")
    .replace(/ແ/g, "แ")
    .replace(/ໂ/g, "โ")
    .replace(/ໃ/g, "ใ")
    .replace(/ໄ/g, "ไ")
    .replace(/ໆ/g, "ๆ")
    .replace(/ໍ່/g, "่อ")
    .replace(/ໍ້/g, "้อ")
    .replace(/ໍ໊/g, "๊อ")
    .replace(/ໍ໋/g, "๋อ")
    .replace(/ໍ/g, "อ")
    .replace(/່/g, "่")
    .replace(/້/g, "้")
    .replace(/໊/g, "๊")
    .replace(/໋/g, "๋")
    .replace(/໌/g, "์")
    .replace(/໎/g, "๎")
    .replace(/໐/g, "๐")
    .replace(/໑/g, "๑")
    .replace(/໒/g, "๒")
    .replace(/໓/g, "๓")
    .replace(/໔/g, "๔")
    .replace(/໕/g, "๕")
    .replace(/໖/g, "๖")
    .replace(/໗/g, "๗")
    .replace(/໘/g, "๘")
    .replace(/໙/g, "๙")
    .replace(/ຯ/g, "ฯ")
    .replace(/໚/g, "๚")
    .replace(/໛/g, "๛")
    .replace(/໏/g, "๏")
    .replace(/\u0EE6/g, "กว")
    .replace(/\u0EE7/g, "ขว")
    .replace(/\u0EE8/g, "คว");

  console.log(text);
});
